const Input = document.getElementById("Input");
const lista = document.getElementById("lista");

// Botão que adiciona texto na lista
function addtxt() {
    // Declarando posição na lista
    var posit = document.getElementById("position")
    var pos = posit.value
    
    // Criando elemento com texto do input 
    const txt = document.createElement('li');
    txt.innerText = Input.value;

    // Fazendo elemento sumir quando é clicado
    txt.addEventListener('click', some);

    // Caso a posição seja invalida adiciona elemento no final da lista
    const tamanho = lista.childElementCount;
    if(!pos ||  pos > tamanho || tamanho <= 0){
    lista.appendChild(txt);
    }else{
    // Adiciona elemento na posição desejada
    let add = lista.children[pos - 1]  
    console.log(add)  
    add.before(txt)      
    }

    //Retoma valores da posição e do input
    Input.value = "";
    posit.value = ""
     
}

// Botão que edita texto na lista
function edittxt(){
    // Declarando posição na lista
    var posit = document.getElementById("position")
    var pos = posit.value

    // Criando elemento com texto do input
    const txt = document.createElement('li');
    txt.innerText = Input.value;

    let add = lista.children[pos - 1]  

    // Fazendo elemento sumir quando é clicado
    txt.addEventListener('click', some);

    // Caso a posição seja invalida edita o elemento no final da lista
    const tamanho = lista.childElementCount;
    if(!pos ||  pos > tamanho || tamanho <= 0){
        lista.lastElementChild.innerHTML = txt.innerText
    }else{
        add.innerHTML = txt.innerText
    } 

    //Retoma valores da posição e do input
    Input.value = "";
    posit.value = ""
}

// função para fazer elemento sumir
function some(e) {
    e.target.remove();
}